// Require dependencies
var lockController = require('./lib/gui/LockController.min.js').bind(this)();
var Mesh = require('./lib/engine/VoxelMesh.min.js');
var Voxel = require('voxel');
var KingTerrain = require('./lib/engine/KingTerrain.min.js');
var PlayerController = require('./lib/engine/PlayerController.js');

// Build the scene
var scene = new THREE.Scene();

// Set up the render surface
renderer = new THREE.WebGLRenderer();
renderer.setClearColor(0xffffff);
renderer.setPixelRatio(window.devicePixelRatio);
renderer.setSize(window.innerWidth, window.innerHeight);
var onWindowResize = function () {
    player.camera.aspect = window.innerWidth / window.innerHeight;
    player.camera.updateProjectionMatrix();
    renderer.setSize(window.innerWidth, window.innerHeight);
}.bind(this);

window.addEventListener('resize', onWindowResize, false);
document.body.appendChild(renderer.domElement);

// Set up the player controller
var player = new PlayerController();

// Set up the terrain
var terrainGen = new KingTerrain();
var voxelData = Voxel.generate([0, 0, 0], [128, 64, 128], terrainGen.getBlock);
var mesh = new Mesh({
    voxels: voxelData.data,
    dims: voxelData.shape
}, Voxel.meshers.greedy);
mesh.createSurfaceMesh();
var material = new THREE.MeshLambertMaterial({
    color: 0x00ff00
});

var terrain = new THREE.Mesh(mesh.geometry, material);
terrain.position.y = -20;
// Set up the environment
var directionalLight = new THREE.DirectionalLight(0xffffff, 0.5);

// Construct the scene
scene.add(directionalLight);
scene.add(terrain);
scene.add(player.getObject());

// Set up animation loop
var animate = function () {
    requestAnimationFrame(animate);
    player.update();
    renderer.render(scene, player.camera);
}.bind(this);
animate();