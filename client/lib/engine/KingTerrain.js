var SimplexNoise = require('simplex-noise');
var alea = require('alea');
var simplex = new SimplexNoise(new alea(new Date().getTime()));
var KingTerrain = function () {

};

noise = function (x, z) {
    return simplex.noise2D(x, z) / 2 + 0.5;
};

KingTerrain.prototype.getBlock = function (x, y, z) {
    var nx = x / (32 / 0.2) - 0.5;
    var nz = z / (32 / 0.2) - 0.5;
    if (y < (noise(nx, nz) * 32)) {
        return 1 * 0xffffff;
    } else {
        return 0;
    }
};

module.exports = KingTerrain;