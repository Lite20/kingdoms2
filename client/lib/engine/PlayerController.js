var PlayerController = function () {
    this.moveForward = false;
    this.moveBackward = false;
    this.moveLeft = false;
    this.moveRight = false;
    this.canJump = false;
    this.prevTime = performance.now();
    this.velocity = new THREE.Vector3();
    this.camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 1, 1000);
    this.controls = new THREE.PointerLockControls(this.camera);
    this.onKeyDown = function (event) {
        switch (event.keyCode) {
        case 38: // up
        case 87: // w
            this.moveForward = true;
            break;
        case 37: // left
        case 65: // a
            this.moveLeft = true;
            break;
        case 40: // down
        case 83: // s
            this.moveBackward = true;
            break;
        case 39: // right
        case 68: // d
            this.moveRight = true;
            break;
        case 32: // space
            if (this.canJump === true) this.velocity.y += 350;
            this.canJump = false;
            break;
        case 27:
            this.controls.enabled = false;
            this.controlsEnabled = false;
            // Hack to co-operate with the LockController
            document.exitPointerLock();
        }
    }.bind(this);
    this.onKeyUp = function (event) {
        switch (event.keyCode) {
        case 38: // up
        case 87: // w
            this.moveForward = false;
            break;
        case 37: // left
        case 65: // a
            this.moveLeft = false;
            break;
        case 40: // down
        case 83: // s
            this.moveBackward = false;
            break;
        case 39: // right
        case 68: // d
            this.moveRight = false;
            break;
        }
    }.bind(this);
    document.addEventListener('keydown', this.onKeyDown, false);
    document.addEventListener('keyup', this.onKeyUp, false);
};

PlayerController.prototype.getObject = function () {
    return this.controls.getObject();
};

PlayerController.prototype.update = function () {
    if (this.controlsEnabled) {
        // TODO check if colliding with terrain VIA raycast down
        var isOnObject = true;
        var time = performance.now();
        var delta = (time - this.prevTime) / 1000;
        this.velocity.x -= this.velocity.x * 10.0 * delta;
        this.velocity.z -= this.velocity.z * 10.0 * delta;
        this.velocity.y -= 9.8 * 100.0 * delta; // 100.0 = mass
        if (this.moveForward) this.velocity.z -= 400.0 * delta;
        if (this.moveBackward) this.velocity.z += 400.0 * delta;
        if (this.moveLeft) this.velocity.x -= 400.0 * delta;
        if (this.moveRight) this.velocity.x += 400.0 * delta;
        if (this.isOnObject === true) {
            this.velocity.y = Math.max(0, this.velocity.y);
            this.canJump = true;
        }

        this.controls.getObject().translateX(this.velocity.x * delta);
        this.controls.getObject().translateY(this.velocity.y * delta);
        this.controls.getObject().translateZ(this.velocity.z * delta);
        if (this.controls.getObject().position.y < 10) {
            this.velocity.y = 0;
            this.controls.getObject().position.y = 10;
            this.canJump = true;
        }

        this.prevTime = time;
    }
};

module.exports = PlayerController;