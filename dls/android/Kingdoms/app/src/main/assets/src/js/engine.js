var engine = {
    goFullscreen: function() {
        if (window.system) {
            if (window.system.platform() == 'android') {

            } else if (window.system.platform() == 'pc') {

            }
        } else {
            if (!screenfull.isFullscreen) {
                swal({
                    title: 'Please go fullscreen',
                    text: 'The experience just isn\'t the same if you \'re not.',
                    type: 'info',
                    showCancelButton: true,
                    confirmButtonText: 'Okay',
                    closeOnConfirm: true
                }, function() {
                    if (screenfull.enabled) {
                        screenfull.request();
                    }
                });
            }
        }
    },
    graphics: {},
    gs: {
        scrollZone: function(xx, yy, ww, hh, space_w, space_h, tch) {
            this.cc = document.createElement('CANVAS');
            this.cc.width = engine.c.width * space_w;
            this.cc.height = engine.c.height * space_h;
            document.getElementsByTagName("body")[0].appendChild(this.cc);
            console.log("Created scroll zone '" + this.cc.width + " by " + this.cc.height + " pixels @(" + (engine.c.height * xx) + ", " + (engine.c.width * yy) + ")");
            this.cctx = this.cc.getContext('2d');
            this.ww = ww;
            this.hh = hh;
            this.xx = xx;
            this.yy = yy;
            this.down = false;
            this.scroll_x = 0; // amount scrolled on x axis
            this.scroll_y = 0; // amount scrolled on y axis
            var stageDrag = function(x, y) {
                this.scroll_x += x - this.statX;
                this.scroll_y += y - this.statY;
            }.bind(this);
            var rel = function(evt) {
                this.down = false;
            }.bind(this);
            var toc = function(evt) {
                this.down = true;
                if (engine.query.debug) {
                    engine.log('info', 'Testing press @' + evt.x + ':' + evt.y + ' against scroll zone @:' + (engine.c.width * xx) + ':' + (engine.c.height * yy) + '-' + ((engine.c.width * xx) + (engine.c.width * ww)) + ':' + ((engine.c.height * yy) + (engine.c.height * hh)));
                }

                if (evt.x > (engine.c.width * xx)) {
                    if (evt.x < ((engine.c.width * xx) + (engine.c.width * ww))) {
                        if (evt.y > (engine.c.height * yy)) {
                            if (evt.y < ((engine.c.height * yy) + (engine.c.height * hh))) {
                                if (engine.query.debug) {
                                    engine.log('info', 'Scroll zone pressed @' + evt.x + ':' + evt.y);
                                }

                                if (!tch(evt, evt.x - (engine.c.width * xx) + this.scroll_x, evt.y - (engine.c.height * yy) + this.scroll_y)) {
                                    stageDrag(evt.x, evt.y);
                                }
                            }
                        }
                    }
                }
            }.bind(this);
            var drg = function(evt) {
                if (this.down) {
                    if (engine.query.debug) {
                        engine.log('info', 'Testing drag @' + evt.x + ':' + evt.y + ' against scroll zone @:' + (engine.c.width * xx) + ':' + (engine.c.height * yy) + '-' + ((engine.c.width * xx) + (engine.c.width * ww)) + ':' + ((engine.c.height * yy) + (engine.c.height * hh)));
                    }

                    if (evt.x > xx) {
                        if (evt.x < (xx + ww)) {
                            if (evt.y > yy) {
                                if (evt.y < (yy + hh)) {
                                    if (engine.query.debug) {
                                        engine.log('info', 'Scroll zone dragged @' + evt.x + ':' + evt.y);
                                    }

                                    if (!tch(evt, (xx - this.xx) + this.scroll_x, (yy - this.yy) + this.scroll_y)) {
                                        stageDrag(evt.x, evt.y);
                                    }
                                }
                            }
                        }
                    }
                }
            }.bind(this);
            engine.on('release', rel);
            engine.on('touch', toc);
            engine.on('drag', drg);
        }
    },
    helpers: {
        socket: function(iip, pport) {
            this.ip = iip;
            this.port = pport;
            this.trusted = {};
            this.hazard = {};
            this.id = UUID();
            this.syncDel = 250;
            this.sync = {};
            this.att = 0; // used to store the ID of countdown to next request timeout
            this.sock = new window.system.socket(4); // use ipv4 protocol
            this.trusted[this.ip + ":" + this.port] = true; // trust the server we're connecting to
            var msghandle = function(input, ip, port) {
                var resp = input.split("/");
                if (this.hazard[ip + ':' + port]) {
                    console.log('A hazard sent this packet: ' + resp + '. Hazard identified as ' + ip + ':' + port);
                    return;
                }

                if (resp[0] == engine.MSG.RESPONSE.AUTHENTICATED) {
                    window.clearTimeout(this.att);
                    if (this.trusted[ip + ':' + port]) {
                        console.log('Client sucessfully created session with ' + this.ip + ':' + this.port);
                    } else {
                        this.hazard[ip + ':' + port] = true;
                        console.log('A system other than the requested server attempted to send an auth accepted packet! Hacker? Marked as hazardous.');
                    }
                } else if (resp[0] == engine.MSG.REQUEST.TRUST) {
                    if (this.trusted[ip + ':' + port]) {
                        this.trusted[resp[1]] = true;
                        console.log(resp[1] + ' has been added to your list of trusted connections for this session.');
                    } else {
                        this.hazard[ip + ':' + port] = true;
                        console.log('The untrusted source @' + ip + ':' + port + ' is trying to get this client to take commands! Hacker? Marked as hazardous');
                    }
                } else if (resp[0] == engine.MSG.REQUEST.SYNC) {
                    if (this.trusted[ip + ':' + port]) {
                        this.trusted[resp[1]] = true;
                        console.log(resp[1] + ' has been added to your list of trusted connections for this session.');
                        var seg = resp[1].split(":");
                        this.sync.ip = seg[0];
                        this.sync.port = seg[1];
                        this.syncDel = resp[2] || 1000;
                        this.sock.sendMsg(engine.MSG.REQUEST.REGISTER + '/' + this.id, this.sync.ip, this.sync.port);
                        console.log("The synchronization server was set to " + resp[1] + " as requested by a server.");
                    } else {
                        this.hazard[ip + ':' + port] = true;
                        console.log('The untrusted source @' + ip + ':' + port + ' is trying to get this client to take commands! Hacker? Marked as hazardous.');
                    }
                } else if (resp[0] == engine.MSG.REQUEST.FETCH) {
                    if (trusted[ip + ':' + port]) {
                        if (resp[1] == '0') { // list
                            window.system.fetch(resp[2], function(resp) {
                                if (resp) {
                                    var list = JSON.parse(resp);
                                    for (var el in list) {
                                        window.system.get(list[el].url, function(err) {
                                            this.sock.sendMsg(engine.MSG.REQUEST.FETCH + '/0/' + resp[3] + '/' + this.id, this.sync.ip, this.sync.port);
                                        });
                                    }
                                    // inform server of completion
                                    this.sock.sendMsg(engine.MSG.REQUEST.FETCH + '/1/' + resp[3] + '/' + this.id, this.sync.ip, this.sync.port);
                                } else {
                                    this.sock.sendMsg(engine.MSG.REQUEST.FETCH + '/0/' + resp[3] + '/' + this.id, this.sync.ip, this.sync.port);
                                    console.log("Failed to fetch dl list from " + resp[2]);
                                }
                            });
                        } else { // item

                        }
                    } else {
                        this.hazard[ip + ':' + port] = true;
                        console.log('The untrusted source @' + ip + ':' + port + ' is trying to get this client to take commands! Hacker? Marked as hazardous.');
                    }
                } else if (resp[0] == engine.MSG.COMMAND.DISCONNECT) {
                    this.emitEvent(engine.MSG.COMMAND.DISCONNECT);
                    console.log('The client was disconnected. Reason: ' + resp[1]);
                } else if (resp[0] == engine.MSG.REQUEST.REFRESHRATE) {
                    if (trusted[ip + ':' + port]) {
                        this.syncDel = Number(resp[1]);
                        console.log('Refresh rate (sync delay) set to ' + resp[1]);
                    } else {
                        this.hazard[ip + ':' + port] = true;
                        console.log('The untrusted source @' + ip + ':' + port + ' is trying to get this client to take commands! Hacker? Marked as hazardous.');
                    }
                } else {
                    // TODO handle unknown packet
                }
            }.bind(this);
            this.sock.addListener('msg', msghandle);
            var syncFunc = function() {
                if (this.sync.data !== {}) { // only sync data if there's anything to sync
                    this.sock.sendMsg(engine.MSG.REQUEST.SYNC + "/" + JSON.stringify(this.sync.data), this.sync.ip, this.sync.port);
                    this.sync.data = {};
                    console.log('Syncronized with server.');
                }

                setTimeout(syncFunc, this.syncDel);
            }.bind(this);
            setTimeout(syncFunc, this.syncDel);
        }
    },
    listeners: {
        touch: [],
        drag: [],
        release: []
    },
    loadprogress: 0,
    log: function(type, msg) {
        console.log('[' + type + '] ' + msg);
    },
    loops: {},
    main: function(tFrame) {
        engine.stopMain = window.requestAnimationFrame(engine.main);
        engine.update(tFrame);
        engine.render();
    },
    menu: {},
    on: function(event, callback) {
        if (!engine.listeners[event]) {
            engine.listeners[event] = [];
        }

        engine.listeners[event][engine.listeners[event].length] = callback;
    },
    render: function() {},
    select: {
        selected: 0
    },
    servers: [],
    stargrid: [],
    views: {},
    version: 'v0.7.5 alpha'
};

heir.inherit(engine.helpers.socket, EventEmitter);
engine.helpers.socket.prototype.connect = function() {
    var fail = function() {
        // no response after sending connection packets twice
        this.emitEvent(engine.MSG.ERROR.CONNECTION_TIMEOUT, [this.ip, this.port]);
        console.log('Attempted to connect to server @' + this.ip + ':' + this.port + ' but the requests were not acknowledged.');
    }.bind(this);
    var attempt = function() {
        this.sock.sendMsg(engine.MSG.REQUEST.REGISTER + "/" + this.id, this.ip, this.port);
        this.att = setTimeout(fail, 5000); // try again
    }.bind(this);
    this.att = setTimeout(attempt, 5000);
    this.sock.sendMsg(engine.MSG.REQUEST.REGISTER + "/" + this.id, this.ip, this.port);
};

engine.helpers.socket.prototype.disconnect = function() {
    // TODO fire disconnect packet
};

engine.gs.scrollZone.prototype.draw = function() {
    // draw content
    engine.ctx.drawImage(this.cc, 0, 0, (engine.c.width * this.ww) + this.scroll_x, (engine.c.height * this.hh) + this.scroll_y, engine.c.width * this.xx, engine.height * this.yy, engine.c.width * this.ww, engine.c.height * this.hh);
    // draw scroll bar
    engine.ctx.fillStyle = 'rgba(255, 255, 255, 0.2);';
    engine.ctx.fillRect((engine.c.width * this.ww) - (engine.c.width / 50), engine.c.height * this.yy, engine.c.width / 50, engine.c.height * this.hh);
};

engine.query = function() {
    var query_string = {};
    var query = window.location.search.substring(1);
    var vars = query.split('&');
    for (var i = 0; i < vars.length; i++) {
        var pair = vars[i].split('=');
        if (typeof query_string[pair[0]] === 'undefined') {
            query_string[pair[0]] = decodeURIComponent(pair[1]);
        } else if (typeof query_string[pair[0]] === 'string') {
            var arr = [query_string[pair[0]], decodeURIComponent(pair[1])];
            query_string[pair[0]] = arr;
        } else {
            query_string[pair[0]].push(decodeURIComponent(pair[1]));
        }
    }

    return query_string;
}();

engine.buildStarGrid = function() {
    engine.stargrid[engine.stargrid.length] = {
        x: 0,
        y: engine.getRandomInt(0, (engine.c.height))
    };

    if (engine.stargrid.length >= 20) {
        engine.stargrid.splice(0, engine.stargrid.length / 2);
    }

    setTimeout(engine.buildStarGrid, engine.getRandomInt(0, 500));
};

engine.getRandomInt = function(min, max) {
    return Math.floor(Math.random() * (max - min)) + min;
};

engine.fetchImage = function(url, callback) {
    var img = new Image();
    img.addEventListener('load', function() {
        engine.graphics[url] = img;
        if (engine.query.debug) {
            engine.log('info', 'Fetched image: ' + url);
        }
        if (callback) {
            callback(img);
        }
    }, false);
    img.src = url;
};

engine.fetchScript = function(url, callback) {
    $.getScript(url).done(function(script, utextStatus) {
        if (engine.query.debug) {
            engine.log('info', 'Fetched image: ' + url);
        }
        if (callback) {
            callback(true);
        }
    }).fail(function(jqxhr, settings, exception) {
        engine.log('info', 'Failed to fetch script with exception: ' + exception);
        if (callback) {
            callback(false);
        }
    });
};
