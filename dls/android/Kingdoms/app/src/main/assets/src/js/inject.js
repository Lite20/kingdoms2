window.system = {
    platform: function() {
        return "android";
    },
    socket: function(protocol) {
        // TODO enable protocol selection
        this.sock = {};
    },
    get: function(url, cb) {
        window.kAndroEngine.get(url);
        cb();
    },
    load: function(url, cb) {
        cb(window.kAndroEngine.load(url));
    },
    fetch: function(url, cb) {
        cb(window.kAndroEngine.fetch(url));
    },
    set: function(url, params, cb) {
        // TODO make POST request
    }
};

window.system.socket.prototype.sendMsg = function(msg, ip, port) {
    window.kAndroEngine.UDPSend(msg, ip, port);
};
