var can = document.getElementById('cvs');
var rc = function() {
    can.width = window.innerWidth;
    can.height = window.innerHeight;
};

window.addEventListener('resize', rc, false);
rc();

var canvas = oCanvas.create({
    canvas: "#cvs",
    fps: 60,
    background: "#000030"
});

/*
engine.keydownHandle = function(evt) {
    evt.preventDefault();
    if (engine.view === 2) { // select menu
        if (evt.keyCode === 13) { // enter key
            var sv = engine.serverlist[engine.select.selected];
            console.log('Connecting to ' + sv.ip + ':' + sv.port);
            engine.connection = new engine.helpers.socket(sv.ip, sv.port);
            engine.connection.addListener(engine.MSG.COMMAND.DISCONNECT, function() {
                view = 1;
                engine.render = engine.rendermenu;
                engine.update = engine.updatemenu;
                console.log('Connection to server closed by server.');
            });

            engine.connection.addListener(engine.MSG.ERROR.CONNECTION_TIMEOUT, function() {
                alert('The server didn\'t respond! Please try a different server, and check this one out later.');
            });

            engine.connection.connect();
        } else if (evt.keyCode === 38) { // up
            engine.select.selected--;
            if (engine.select.selected < 0) {
                engine.select.selected = engine.serverlist.length - 1;
            }
        } else if (evt.keyCode === 40) { // down
            engine.select.selected++;
            if (engine.select.selected >= engine.serverlist.length) {
                engine.select.selected = 0;
            }
        }
    }
};
*/

engine.update = engine.loops.boot;
engine.render = engine.views.boot;
engine.update();
engine.render();
var upd = function() {
    engine.update();
    window.requestAnimationFrame(upd);
};

var lop = function() {
    engine.render();
    window.requestAnimationFrame(lop);
};
