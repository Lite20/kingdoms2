var KingUI = {};
KingUI.Button = function(x, y, w, h, text, func) {
    this.rect = canvas.display.rectangle({
        x: x * canvas.width,
        y: y * canvas.height,
        width: w * canvas.width,
        height: h * canvas.height,
        fill: "#FFFFFF",
        click: func
    });

    var tex = canvas.display.text({
        x: (x * canvas.width) + ((text.length * 8) + 16),
        y: (y * canvas.height) + (canvas.height / 20),
        origin: {
            x: "center",
            y: "center"
        },
        align: "center",
        font: "bold 16px/1.5 sans-serif",
        text: text,
        fill: "#000"
    });

    this.rect.addChild(tex);
    canvas.addChild(this.rect);
};
