var TILE_WIDTH = 64;
var TILE_HEIGHT = 32;
var TILE_WIDTH_HALF = TILE_WIDTH / 2;
var TILE_HEIGHT_HALF = TILE_HEIGHT / 2;
var IsoKit = {
    screenToMap: function(c) {
        var gy = c.y / TILE_HEIGHT_HALF;
        var gx;
        if(isOdd(Math.floor(gy))) {
            gx = (c.x - TILE_WIDTH_HALF) / TILE_WIDTH;
        } else {
            gx = c.x / TILE_WIDTH;
        }

        return new Coord(gx, gy);
    },
    mapToScreen: function(c) {
        var sx = c.x * TILE_WIDTH;
        if(isOdd(c.y)) {
            sx += TILE_WIDTH_HALF;
        }

        var sy = c.y * TILE_HEIGHT_HALF;
        return new Coord(sx, sy);
    },
    bakeLayer: function(map, tileset) {
        buffer = document.createElement('CANVAS');
        buffer.width = map.width * TILE_WIDTH;
        buffer.height = map.height * TILE_HEIGHT_HALF; // this axis is only half as tall because it's staggered
        buff = buffer.getContext('2d');
        for(var y = 0; y < map.height; y += 2) {
            for(var x = 0; x < map.width; x++) {
                if(isOdd(y)) {
                    buff.drawImage(tileset[map.data[(y * map.width) + x]], (x * TILE_WIDTH) + TILE_WIDTH_HALF, y * TILE_HEIGHT_HALF);
                } else {
                    buff.drawImage(tileset[map.data[(y * map.width) + x]], x * TILE_WIDTH, y * TILE_HEIGHT_HALF);
                }
            }
        }

        return buffer;
    }
};

function isOdd(num) {
    return (num % 2) == 1;
}

var Coord = function(x, y) {
    this.x = x;
    this.y = y;
};
