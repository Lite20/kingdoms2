var MSG = {
    RESPONSE: {
        AUTHENTICATED: "1"
    },
    REQUEST: {
        SYNC: "2",
        TRUST: "3",
        REGISTER: "4",
        LOADMAP: "5",
        STATUS: "6",
        RENDERMAP: "8",
        FETCH: "10",
        REFRESHRATE: "11"
    },
    COMMAND: {
        DISCONNECT: "0",
        SET_POS: "9"
    },
    ERROR: {
        CONNECTION_TIMEOUT: "7"
    }
};
