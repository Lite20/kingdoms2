var canvas = oCanvas.create({
    canvas: "#cvs",
    fps: 60,
    background: "#000030"
});

var btn = function(x, y, text) {
    this.rect = canvas.display.rectangle({
        x: x,
        y: y,
        width: (text.length * 16) + 32,
        height: canvas.height / 10,
        fill: "#000030"
    });

    var tex = canvas.display.text({
        x: x + ((text.length * 8) + 16),
        y: y + (canvas.height / 20),
        origin: {
            x: "center",
            y: "center"
        },
        align: "center",
        font: "bold 16px/1.5 sans-serif",
        text: text,
        fill: "#000"
    });

this.rect.addChild(tex);
canvas.addChild(this.rect);
};

var go = new btn(40, 40, "GO!");
