engine.views.boot = function() {
    var text = canvas.display.text({
    	x: canvas.width / 2,
    	y: canvas.height / 2,
    	origin: { x: "center", y: "top" },
    	font: "bold " + (engine.c.height / 7) + "px Revalia",
    	text: "GameFruit",
    	fill: "#FFFFFF"
    });

    canvas.addChild(text);
};

engine.loops.boot = function() {
    if (engine.loadprogress >= 100) {
        engine.buildStarGrid();
        engine.view = 1;
        engine.render = engine.rendermenu;
        engine.update = engine.updatemenu;
    } else if (engine.loadprogress == 10) {
        if (engine.query.debug) {
            window.onerror = function(msg, url, linenumber) {
                alert('Kingdoms Error: ' + msg + '\nURL: ' + url + '\nLine Number: ' + linenumber);
                return false;
            };

            engine.fpsmeter = new FPSMeter({
                graph: 1,
                history: 20
            });

            engine.main = function(tFrame) {
                engine.stopMain = window.requestAnimationFrame(engine.main);
                engine.update(tFrame);
                engine.render();
                engine.fpsmeter.tick();
            };

            engine.on('touch', function(evt) {
                engine.log('info', evt);
            });

            engine.log('info', 'Debug mode enabled. ');
        }
    } else if (engine.loadprogress == 20) {
        engine.fetchImage('./assets/full_logo.png', function() {
            engine.loadprogress++;
        });
    } else if (engine.loadprogress == 30) {
        window.system.fetch('http://45.55.194.192/servers.json', function(val) {
            var ee = JSON.parse(val);
            engine.serverlist = ee.servers;
            engine.log('info', 'Server list loaded');
            engine.loadprogress++;
        });

        engine.loadprogress++;
    } else if (engine.loadprogress == 31) {
        // sit and wait for server list
    } else if (engine.loadprogress == 40) {
        engine.menu.playButton = new engine.gs.button('Play', 0.3, 0.4, 0.3, 0.1, function() {
            engine.render = engine.renderSelect;
            engine.update = engine.updateSelect;
            engine.view = 2;
        });

        engine.menu.settingsButton = new engine.gs.button('Settings', 0.3, 0.51, 0.3, 0.1, function() {
            alert('Not built yet!');
        });

        engine.select.backButton = new engine.gs.button('Back', 0.01, 0.01, 0.2, 1 / 15, function() {
            engine.render = engine.rendermenu;
            engine.update = engine.updatemenu;
            engine.view = 1;
        });

        engine.loadprogress++;
    } else if (engine.loadprogress == 50) {
        engine.select.slide = new engine.gs.scrollZone(0.05, 0.1, (2 / 5) - 0.05, 0.6, (2 / 5) - 0.05, 1, function(evt, x, y) {
            // TODO handle touch events
            return false;
        });

        engine.select.slide.cctx.textAlign = 'left';
        engine.select.slide.cctx.font = (engine.c.height / 24) + 'px Revalia';
        for (var zx = 0; zx < engine.serverlist.length - 1; zx++) {
            if (zx == engine.select.selected) {
                engine.select.slide.cctx.fillStyle = '#FFFFFF';
            } else {
                engine.select.slide.cctx.fillStyle = 'rgba(255, 255, 255, 0.6)';
            }

            engine.select.slide.cctx.fillRect(0, ((engine.c.height / 15) * zx) + (engine.c.height / 5), engine.select.slide.cc.width, engine.select.slide.cc.height / 16);
            if (zx == engine.select.selected) {
                engine.select.slide.cctx.fillStyle = '#000000';
            } else {
                engine.select.slide.cctx.fillStyle = '#FFFFFF';
            }
        }

        engine.select.slide.cctx.fillText(engine.serverlist[zx].name, (engine.c.width / 50), (((engine.c.height / 15) * zx) + (engine.c.height / 5)) + (engine.c.height / 24));
        engine.loadprogress++;
    } else if (engine.loadprogress == 90) {
        engine.goFullscreen();
        engine.loadprogress++;
    } else { // if there's no task, move along
        engine.loadprogress++;
    }
};
