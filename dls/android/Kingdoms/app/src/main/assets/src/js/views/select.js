engine.views.select = function() {
    engine.ctx.fillStyle = '#000030';
    engine.ctx.fillRect(0, 0, engine.c.width, engine.c.height);
    engine.ctx.fillStyle = '#FFFFFF';
    engine.ctx.textAlign = 'left';
    engine.ctx.font = '16px Revalia';
    engine.ctx.fillText(engine.version, 2, 18);
    for (var i = 0; i < engine.stargrid.length; i++) {
        var x = engine.stargrid[i].x;
        var y = engine.stargrid[i].y;
        engine.ctx.fillStyle = 'rgba(255, 255, 255, 1)';
        engine.ctx.fillRect(x, y, 10, 10);
        engine.ctx.fillStyle = 'rgba(255, 255, 255, 0.8)';
        engine.ctx.fillRect(x - 10, y, 10, 10);
        engine.ctx.fillStyle = 'rgba(255, 255, 255, 0.6)';
        engine.ctx.fillRect(x - 20, y, 10, 10);
        engine.ctx.fillStyle = 'rgba(255, 255, 255, 0.4)';
        engine.ctx.fillRect(x - 30, y, 10, 10);
        engine.ctx.fillStyle = 'rgba(255, 255, 255, 0.2)';
        engine.ctx.fillRect(x - 40, y, 10, 10);
    }

    engine.select.backButton.draw();
    engine.select.slide.draw();

};

engine.loops.select = function() {
    for (var i = 0; i < engine.stargrid.length; i++) {
        engine.stargrid[i].x += 10;
    }
};
