package info.gamefruit.kingdoms;

import android.app.Activity;
import android.app.NotificationManager;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.NotificationCompat;
import android.webkit.JavascriptInterface;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.URL;
import java.nio.charset.Charset;

/**
 * Created by lite on 20/12/15.
 */
public class kAndroEngine {
    private int ids = 0;
    private Activity activity;
    private NotificationCompat.Builder mBuilder;
    private NotificationManager notifier;
    private DatagramSocket ds;
    public kAndroEngine(Activity ac) {
        activity = ac;
        try {
            ds = new DatagramSocket();
        } catch (SocketException e) {
            Toast.makeText(activity, e.getMessage(), Toast.LENGTH_LONG).show();
        }

        this.mBuilder = new NotificationCompat.Builder(activity);
        this.notifier = (NotificationManager) activity.getSystemService(Context.NOTIFICATION_SERVICE);
    }

    @android.webkit.JavascriptInterface
    public void get(String uurl) {
        FileOutputStream out = null;
        InputStream in = null;
        try {
            String[] p = uurl.split("/");
            String name = p[p.length - 1];
            File file = new File(activity.getApplicationContext().getFilesDir() + "//cache//" + p[2] + "//" + name, uurl);
            out = new FileOutputStream(file);
            ConnectivityManager conmgr = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netinfo = conmgr.getActiveNetworkInfo();
            if(netinfo != null && netinfo.isConnected()) {
                InputStream is = null;
                URL url = new URL(uurl);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(10000);
                conn.setConnectTimeout(15000);
                conn.setRequestMethod("GET");
                conn.setDoInput(true);
                conn.connect();
                in = conn.getInputStream();
                int c;
                while ((c = in.read()) != -1) {
                    out.write(c);
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (out != null) {
                try {
                    out.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @android.webkit.JavascriptInterface
    public String load(String url) {
        try {
            String[] p = url.split("/");
            String name = p[p.length - 1];
            File file = new File(activity.getApplicationContext().getFilesDir() + "//cache//" + p[2] + "//" + name, url);
            FileInputStream fis = activity.getApplicationContext().openFileInput(file.getPath());
            InputStreamReader isr = new InputStreamReader(fis);
            BufferedReader bufferedReader = new BufferedReader(isr);
            StringBuilder sb = new StringBuilder();
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                sb.append(line);
            }

            return sb.toString();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    @android.webkit.JavascriptInterface
    public void toast(String message, Boolean longmsg) {
        if(longmsg) {
            Toast.makeText(activity, message, Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(activity, message, Toast.LENGTH_SHORT).show();
        }
    }

    @android.webkit.JavascriptInterface
    public int notification(String title, String text) {
        mBuilder.setSmallIcon(R.drawable.ic_stat_name);
        mBuilder.setContentTitle(title);
        mBuilder.setContentText(text);
        int id = ids;
        ids++;
        notifier.notify(id, mBuilder.build());
        return id;
    }

    @android.webkit.JavascriptInterface
    public String fetch(String u) throws Exception {
        ConnectivityManager conmgr = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netinfo = conmgr.getActiveNetworkInfo();
        if(netinfo != null && netinfo.isConnected()) {
            InputStream is = null;
            URL url = new URL(u);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(15000);
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            conn.connect();
            BufferedReader r = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            StringBuilder total = new StringBuilder();
            String line;
            while ((line = r.readLine()) != null) {
                total.append(line).append('\n');
            }

            conn.getInputStream().close();
            return total.toString();
        } else {
            return null; // no wifi
        }
    }

    @android.webkit.JavascriptInterface
    public void UDPSend(String msg, String ip, int port) {
        try {
            byte[] buff = msg.getBytes(Charset.forName("UTF-8"));
            ds.send(new DatagramPacket(buff, 0, buff.length, InetAddress.getByName(ip), port));
        } catch (IOException e) {
            Toast.makeText(activity, e.getMessage(), Toast.LENGTH_LONG).show();
        }
    }
}