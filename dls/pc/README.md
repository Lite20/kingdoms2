# How to Run PC Download Edition from repo

In order to use this you must make a symlink named "src" in this directory that points to the "client" folder in the root directory. This is doable on both Linux and Windows
but has not been confirmed on Mac.

Once you've made the symlink, you can run with "npm start"