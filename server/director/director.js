require('colors');
var fs = require('fs');
var express = require('express');
var app = express();
var dgram = require('dgram');
var config = require('./config.json');
var EventEmitter = require('wolfy87-eventemitter');
var heir = require('heir');
ENGINE = {};
var sock4;
var sock6;
var clients = {};
var plugins = {};
ENGINE.MSG = {
    RESPONSE: {
        AUTHENTICATED: "1"
    },
    REQUEST: {
        SYNC: "2",
        TRUST: "3",
        REGISTER: "4",
        LOADMAP: "5",
        STATUS: "6",
        RENDERMAP: "8",
        FETCH: "10",
        REFRESHRATE: "11"
    },
    COMMAND: {
        DISCONNECT: "0",
        SET_POS: "9"
    },
    ERROR: {
        CONNECTION_TIMEOUT: "7"
    }
};

ENGINE.Director = new EventEmitter();
fs.readdir(__dirname + '/plugins/', function(err, data) {
    if(err) {
        console.log(err);
        return;
    }

    var loaded = 0;
    var name = "";
    var plug;
    for(var pl in data) {
        plug = require(__dirname + '/plugins/' + data[pl] + "/");
        pack = require(__dirname + '/plugins/' + data[pl] + "/package.json");
        name = plug.getName();
        plug.init({
            log: function(msg) {
                console.log(("[" + name + "] ").blue + msg);
            }
        });

        plugins[name] = plug;
        loaded++;
        console.log(info() + " Running " + data[pl].yellow + " version " + pack.version.yellow + " as " + name.yellow);
    }

    if(loaded == 1) {
        console.log(info() + " Loaded " + "1".yellow + " plugin");
    } else {
        console.log(info() + " Loaded " + (loaded + "").yellow + " plugins");
    }
});

app.use(express.static('public'));
app.listen(config.serving.port, function() {
    console.log(info() + ' File serving on port: '.green + config.serving.port);
});

if (!config.ipv4 && !config.ipv6) {
    console.log(info() + '[FATAL] Both ipv4 and ipv6 are disabled! Please enable one, or both!'.red);
    console.log(info() + '[FATAL] Terminating...'.red);
    process.exit(1);
}

var reqHandle = function(msg, rinfo) {
    var req = msg.toString().split("/");
    if (req[0] == ENGINE.MSG.REQUEST.REGISTER) { // register command
        if (req.length < 2) { // invalid number of params
            return;
        }

        if (clients[req[1]]) { // if client already registered exist
            if (clients[req[1]].address == rinfo.address) { // the registered client has the same IP
                // client has re-attempted to connect. Last packet acknowledging it must have failed
                ENGINE.send(ENGINE.MSG.RESPONSE.AUTHENTICATED, rinfo); // inform the client they've been registered
            } else {
                // disconnect the old client
                ENGINE.send(ENGINE.MSG.COMMAND.DISCONNECT + '/Another client connected in your name.', clients[req[1]].rinfo);
                ENGINE.Director.emitEvent(ENGINE.MSG.COMMAND.DISCONNECT, [req[1]]);
                clients[req[1]] = { // update to the new client
                    rinfo: rinfo,
                    new: true
                };

                ENGINE.Director.emitEvent(ENGINE.MSG.RESPONSE.AUTHENTICATED, [rinfo]);
                console.log(info() + "[WARNING] A client was disconnected and replaced!");
                console.log(info() + "[WARNING] It was probably just a double ping.");
            }
        } else {
            clients[req[1]] = { // register the newcomer
                rinfo: rinfo,
                new: true
            };

            ENGINE.send(ENGINE.MSG.RESPONSE.AUTHENTICATED, rinfo); // inform the client they've been registered
            ENGINE.send(ENGINE.MSG.REQUEST.SYNC + "/" + config.hub.ip + ":" + config.hub.port, rinfo); // tell them to connect to the hub
            ENGINE.Director.emitEvent(ENGINE.MSG.RESPONSE.AUTHENTICATED, [req[1]]); // emit a connection event
            console.log(info() + '[INFO] A client connected @' + rinfo.address + ":" + rinfo.port);
        }
    } else if (req[0] == ENGINE.MSG.COMMAND.DISCONNECT) {
        delete clients[req.usr]; // unregister them
        console.log(info() + '[INFO] A client disconnected @' + rinfo.address + ":" + rinfo.port);
    } else if (req[0] == ENGINE.MSG.REQUEST.STATUS) {
        ENGINE.send(ENGINE.MSG.REQUEST.STATUS + '/' + config.status + '/' + config.motd + '/' + config.menuscript, rinfo);
    } else {
        console.log(info() + '[WARNING] Invalid command packet! '.yellow + msg.toString() + ' from ' + rinfo.address + ":" + rinfo.port);
    }
};

if (config.ipv4) {
    sock4 = dgram.createSocket('udp4');
    sock4.on('message', reqHandle);
    sock4.on('error', function(err) {
        console.log(info() + ('[FATAL] Server socket error:\n' + err.stack).red);
        console.log(info() + '[FATAL] Terminating...');
        sock4.close();
        process.exit(1);
    });

    sock4.bind(config.port, function() {
        console.log(info() + (' Server listening @' + sock4.address().address + ':' + sock4.address().port + ' with ' + sock4.address().family).green);
    });
}

if (config.ipv6) {
    sock6 = dgram.createSocket('udp6');
    sock6.on('message', reqHandle);
    sock6.on('error', function(err) {
        console.log(info() + ('[FATAL] Server socket error:\n' + err.stack).red);
        console.log(info() + '[FATAL] Terminating...');
        sock6.close();
        process.exit(1);
    });

    sock6.bind(config.port, function() {
        console.log(info() + ('Server listening @' + sock6.address().address + ':' + sock6.address().port + ' with ' + sock6.address().family).green);
    });
}

setInterval(function() { // check who's still there every 30 seconds
    var evicted = 0;
    for (var client in clients) {
        if (!client.new) { // if they just joined they haven't pinged yet
            clients[client].pings--;
            if (clients[client].pings < 0) { // Timeout! Disconnect the user
                ENGINE.send(ENGINE.MSG.COMMAND.DISCONNECT, client.rinfo);
            }
        } else {
            client.new = false;
        }
    }

    if (evicted > 0) {
        console.log(evicted + ' sessions terminated.'.red);
    }
}, config.ping_interval * 1000);
function info() {
    return '[Server]'.green;
}

ENGINE.send = function(obj, dest) {
    var msg = new Buffer(obj.toString());
    if (dest.family.charAt(3) == 4) { // Ipv4 so use udp4
        sock4.send(msg, 0, msg.length, dest.port, dest.address, function(err) {
            if (err) {
                throw err;
            }
        });
    } else { // Ipv6 so use udp6
        sock6.send(msg, 0, msg.length, dest.port, dest.address, function(err) {
            if (err) {
                throw err;
            }
        });
    }
};
