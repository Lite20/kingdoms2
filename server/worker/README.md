# Status: Work In progress
## Kingdoms workers
The workers are the sub-servers to the director. Workers handle synchronizing player data between players.
Each worker must have worlds assigned to it, and it will handle the players in only those worlds.
