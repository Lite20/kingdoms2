var config = require('./config.json');
var toolkit;
module.exports = {
    init: function(tk) {
        toolkit = tk;
        tk.log("Adding listeners...");
        ENGINE.Worker.addListener(ENGINE.MSG.REQUEST.REGISTER, function(rinfo, id) {
            // make the player load all mandatory dependencies
            ENGINE.send(ENGINE.MSG.REQUEST.FETCH + '/0/' + config.dl.mandatory + '/DLMAND', rinfo);
        });

        ENGINE.Worker.addListener(ENGINE.MSG.REQUEST.FETCH + 'DLMAND', function(rinfo, id) {
            // make the player load the rest in the background
            ENGINE.send(ENGINE.MSG.REQUEST.FETCH + '/0/' + config.dl.mandatory + '/DLMAND', rinfo);
        });
    },
    getName: function() {
        return "core";
    }
};

function info() {
    return '[Core] '.yellow;
}
