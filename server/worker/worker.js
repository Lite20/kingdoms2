require('colors');
var NanoTimer = require('nanotimer');
var timerA = new NanoTimer();
var fs = require('fs');
var dgram = require('dgram');
var config = require('./config.json');
var EventEmitter = require('wolfy87-eventemitter');
var heir = require('heir');
ENGINE = {};
var sock4;
var sock6;
var plugins = {};
var zones = {};
var ploc = {}; // map player ID's to respective worlds
var clients = []; // player ID's
var updates = {};
ENGINE.MSG = {
    RESPONSE: {
        AUTHENTICATED: "1"
    },
    REQUEST: {
        SYNC: "2",
        TRUST: "3",
        REGISTER: "4",
        LOADMAP: "5",
        STATUS: "6",
        RENDERMAP: "8",
        FETCH: "10",
        REFRESHRATE: "11"
    },
    COMMAND: {
        DISCONNECT: "0",
        SET_POS: "9"
    },
    ERROR: {
        CONNECTION_TIMEOUT: "7"
    }
};
// load zones
for(var zone in config.zones) {
    zones[config.zones[zone]] = {};
    console.log(info() + " Loaded zone ".green + config.zones[zone].name);
}

ENGINE.Worker = new EventEmitter();
fs.readdir(__dirname + '/plugins/', function(err, data) {
    if(err) {
        console.log(err);
        return;
    }

    var loaded = 0;
    var name = "";
    var plug;
    for(var pl in data) {
        plug = require(__dirname + '/plugins/' + data[pl] + "/");
        pack = require(__dirname + '/plugins/' + data[pl] + "/package.json");
        name = plug.getName();
        plug.init({
            log: function(msg) {
                console.log(("[" + name + "] ").blue + msg);
            }
        });

        plugins[name] = plug;
        loaded++;
        console.log(info() + " Running " + data[pl].yellow + " version " + pack.version.yellow + " as " + name.yellow);
    }

    if(loaded == 1) {
        console.log(info() + " Loaded " + "1".yellow + " plugin");
    } else {
        console.log(info() + " Loaded " + (loaded + "").yellow + " plugins");
    }
});

if (!config.ipv4 && !config.ipv6) {
    console.log(info() + '[FATAL] Both ipv4 and ipv6 are disabled! Please enable one, or both!'.red);
    console.log(info() + '[FATAL] Terminating...'.red);
    process.exit(1);
}

var reqHandle = function(msg, rinfo) {
    var req = msg.toString().split("/");
    if(req[0] == ENGINE.MSG.COMMAND.SET_POS) { // Params: CMD/ID/POS
        if(req[1]) {
            if(!updates[req[1]]) {
                zones[ploc[req[1]]] = req[2]; // place it in the map
                updates[req[1]] = req[2];
            }
        }
    } else if(req[0] == ENGINE.MSG.REQUEST.REGISTER) {
        if(req[1]) {
            ENGINE.send(ENGINE.MSG.RESPONSE.AUTHENTICATED, rinfo);
            ENGINE.send(ENGINE.MSG.RESPONSE.REFRESHRATE + '/' + config.refreshRate, rinfo);
            console.log(info() + '[INFO] Client ' + req[1] + ' connected @' + rinfo.address + ":" + rinfo.port);
        } else {
            console.log(info() + '[WARNING] Failed to connect a client! (No ID)');
        }
    } else if(req[0] == ENGINE.MSG.REQUEST.FETCH) {
        ENGINE.Worker.emitEvent(ENGINE.MSG.REQUEST.FETCH + req[2], [rinfo, req[1], req[3]]);
    } else {

    }
};

if (config.ipv4) {
    sock4 = dgram.createSocket('udp4');
    sock4.on('message', reqHandle);
    sock4.on('error', function(err) {
        console.log(info() + ('[FATAL] Server socket error:\n' + err.stack).red);
        console.log(info() + '[FATAL] Terminating...');
        sock4.close();
        process.exit(1);
    });

    sock4.bind(config.port, function() {
        console.log(info() + (' Server listening @' + sock4.address().address + ':' + sock4.address().port + ' with ' + sock4.address().family).green);
    });
}

if (config.ipv6) {
    sock6 = dgram.createSocket('udp6');
    sock6.on('message', reqHandle);
    sock6.on('error', function(err) {
        console.log(info() + ('[FATAL] Server socket error:\n' + err.stack).red);
        console.log(info() + '[FATAL] Terminating...');
        sock6.close();
        process.exit(1);
    });

    sock6.bind(config.port, function() {
        console.log(info() + ('Server listening @' + sock6.address().address + ':' + sock6.address().port + ' with ' + sock6.address().family).green);
    });
}

function info() {
    return '[Server]'.green;
}

ENGINE.send = function(obj, dest) {
    var msg = new Buffer(obj.toString());
    if (dest.family.charAt(3) == 4) { // Ipv4 so use udp4
        sock4.send(msg, 0, msg.length, dest.port, dest.address, function(err) {
            if (err) {
                throw err;
            }
        });
    } else { // Ipv6 so use udp6
        sock6.send(msg, 0, msg.length, dest.port, dest.address, function(err) {
            if (err) {
                throw err;
            }
        });
    }
};
/* In case a client sends multiple position updates */
var update = function() {
    var packet = ENGINE.MSG.COMMAND.SET_POS; // create the packet
    for(var upd in updates) {
        packet = packet + '/' + udp + ':' + updates[udp];
    }

    updates = {}; // empty the updates object to be filled again
    echo(packet);
    setTimeout(update, config.refreshRate);
};

var echo = function(packet) {
    for(var cli in clients) {
        ENGINE.send(packet, clients[cli].rinfo);
    }
};

setTimeout(update, config.refreshRate);
